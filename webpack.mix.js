const mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            '@': __dirname + '/resources'
        }
    }
});
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/js/app.js', 'resources/js/demo/sb-admin-2.js'], 'public/build/js/app.js')
    .scripts([
      'resources/js/demo/demo/chart-area-demo.js',
      'resources/js/demo/demo/chart-bar-demo.js',
      'resources/js/demo/demo/chart-pie-demo.js',
      'resources/js/demo/demo/datatables-demo.js',
    ], 'public/build/js/demo/demo.js')
    .scripts([
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
      'public/vendor/jquery-easing/jquery.easing.min.js',
      'node_modules/chart.js/dist/Chart.min.js',
    ], 'public/build/js/vendor.js')
    .sass('resources/sass/app.scss', 'public/build/css')
    .styles([
      'node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css',
    ], 'public/build/css/vendor.css');

mix.copyDirectory('node_modules/@fortawesome/fontawesome-free/svgs', 'public/build/svgs');


  // <!-- Bootstrap core JavaScript-->
  // <script src="vendor/jquery/jquery.min.js"></script>
  // <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  // <!-- Core plugin JavaScript-->
  // <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  // <!-- Custom scripts for all pages-->
  // <script src="js/sb-admin-2.min.js"></script>

  // <!-- Page level plugins -->
  // <script src="vendor/chart.js/Chart.min.js"></script>

  // <!-- Page level custom scripts -->
  // <script src="js/demo/chart-area-demo.js"></script>
  // <script src="js/demo/chart-pie-demo.js"></script>
