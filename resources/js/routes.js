import Vue from 'vue';
import VueRouter from 'vue-router'

import Home from '@/js/components/Home';
import Login from '@/js/components/Auth/Login';
import About from '@/js/components/About';
import Group from '@/js/components/Group';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
              auth: false
            }
        },
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/groups',
            name: 'groups',
            component: Group
        }
    ]
});

export default router;
