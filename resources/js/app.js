require('./bootstrap');

window.Vue = require('vue');
import Routes from '@/js/routes.js';

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

if ($('#app')[0]) {
  console.log('App loading...')
  const app = new Vue({
    el: '#app',
    router: Routes,
  })

  window.vueInstance = app
}
