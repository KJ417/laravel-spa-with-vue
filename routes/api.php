<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Unauthenticated Routes
Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function() {
    /**
     * Auth Routes
     *
     */
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('register', 'RegisterController@register')->name('register');
});

Route::middleware('auth:airlock')->get('/user', function (Request $request) {
    return $request->user();
});
