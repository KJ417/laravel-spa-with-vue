<?php

use Illuminate\Support\Facades\Route;

// Route to handle page loading with Vue except for api and airlock routes
Route::get('/{any?}', function (){
    return view('app');
})->where('any', '^(?!\/api\/)(?!\/airlock\/)[\/\w\.-]*');
