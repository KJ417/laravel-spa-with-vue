<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Admin',
            'last_name' => 'Person',
            'email' => 'admin@example.com',
            'password' => Hash::make('secret'),
        ]);
        User::create([
            'first_name' => 'User',
            'last_name' => 'Person',
            'email' => 'user@example.com',
            'password' => Hash::make('secret'),
        ]);
    }
}
