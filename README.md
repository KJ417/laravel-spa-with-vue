## About this project

This is a demonstration showing how Laravel and Vue can be used to create a working Single Page Application using the SB Admin 2 theme as a starting point.

## TODO

- Include a list of features out of the box
- Include setup instructions

## License

[Laravel](https://laravel.com/), [Vue.js](https://vuejs.org/), and [SB Admin 2](https://startbootstrap.com/themes/sb-admin-2/) are all open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
